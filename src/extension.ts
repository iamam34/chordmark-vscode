import * as vscode from "vscode";
const puppeteer = require("puppeteer");

export function activate(context: vscode.ExtensionContext) {
  let panel: vscode.WebviewPanel | undefined = undefined;

  const outputChannel = vscode.window.createOutputChannel("ChordMark", {
    log: true,
  });

  outputChannel.info("Registered ChordMark extension", context.extension.id);

  vscode.commands.registerCommand("chordmark.exportToPDF", async () => {
    // get file path of currently previewed song from extension context
    const sourceUri: vscode.Uri = context.workspaceState.get(
      "sourceUri"
    ) as vscode.Uri;
    let songHTML = context.workspaceState.get("songHTML") as string;

    if (!panel || !sourceUri) {
      outputChannel.warn("Cannot export without a preview.", panel, sourceUri);
      vscode.window.showWarningMessage(
        "ChordMark: Cannot export without a preview. Try previewing first."
      );
    } else {
      // TODO rename PDF file to match source file
      const destUri = vscode.Uri.joinPath(sourceUri, "..", "output.pdf");

      const browser = await puppeteer.launch({
        headless: "new",
      });
      const page = await browser.newPage();
      let innerHTML = getPuppeteerContent(
        "'*'",
        songHTML,
        vscode.Uri.joinPath(context.extensionUri, "media", "styles.css")
      );
      await page.setContent(innerHTML, {
        timeout: 30000,
        waitUntil: "networkidle0",
      });
      await page.pdf({
        format: "A4",
        path: destUri.fsPath,
      });
      await browser.close();

      // create a new file and write the pdf contents to it
      vscode.window.showInformationMessage(destUri.fsPath);
      outputChannel.info("Saved PDF file to ", destUri.fsPath);
    }
  });

  let disposable = vscode.commands.registerCommand(
    "chordmark.showPreviewToSide",
    () => {
      if (panel) {
        panel.reveal(vscode.ViewColumn.Beside);
      } else {
        panel = vscode.window.createWebviewPanel(
          "chordMarkPreview",
          "ChordMark Preview",
          vscode.ViewColumn.Beside,
          {
            enableScripts: true,
            localResourceRoots: [
              vscode.Uri.joinPath(context.extensionUri, "media"),
            ],
          }
        );

        const stylesUri = panel.webview.asWebviewUri(
          vscode.Uri.joinPath(context.extensionUri, "media", "styles.css")
        );
        const chordMarkScriptUri = panel.webview.asWebviewUri(
          vscode.Uri.joinPath(
            context.extensionUri,
            "media",
            "lib",
            "chord-mark.js"
          )
        );
        const myScriptUri = panel.webview.asWebviewUri(
          vscode.Uri.joinPath(context.extensionUri, "media", "main.js")
        );

        panel.webview.html = getWebviewContent(
          panel.webview.cspSource,
          stylesUri,
          chordMarkScriptUri,
          myScriptUri
        );

        panel.webview.onDidReceiveMessage((message) => {
          switch (message.command) {
            case "saveHTML":
              context.workspaceState.update("songHTML", message.data);
              break;
          }
        });

        panel.onDidDispose(
          () => {
            panel = undefined;
          },
          null,
          context.subscriptions
        );
      }

      let editor = vscode.window.activeTextEditor;
      if (!editor) {
        vscode.window.showWarningMessage(
          "ChordMark: No active text editor, nothing to render."
        );
      } else {
        const sourceUri = editor.document.uri;
        outputChannel.info("Updating sourceUri", sourceUri);
        context.workspaceState.update("sourceUri", sourceUri);
        panel.webview.postMessage({
          command: "renderSong",
          rawSong: editor.document.getText(),
        });
      }
    }
  );

  context.subscriptions.push(disposable);
}

// This method is called when your extension is deactivated
export function deactivate() {}

function getWebviewContent(
  cspSource: string,
  stylesUri: vscode.Uri,
  chordMarkScriptUri: vscode.Uri,
  myScriptUri: vscode.Uri
) {
  // Use a nonce to only allow specific scripts to be run
  const nonce = getNonce();

  return `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Security-Policy" content="default-src 'none'; script-src ${cspSource}; style-src ${cspSource};"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="${stylesUri}" rel="stylesheet">
    <title>ChordMark Preview</title>
</head>
<body>
    <div id="chordmark-song-container" class="cmTheme-print" />
    <script nonce="${nonce}" src="${chordMarkScriptUri}"></script>
    <script nonce="${nonce}" src="${myScriptUri}" type="module"></script>
</body>
</html>`;
}

function getPuppeteerContent(cspSource: string, songHTML: string, stylesUri: vscode.Uri) {
  return `<!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta http-equiv="Content-Security-Policy" content="default-src 'none'; style-src ${cspSource};"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link href="${stylesUri}" rel="stylesheet">
      <title>ChordMark Preview</title>
  </head>
  <body>
      <div id="chordmark-song-container" class="cmTheme-print" >
        ${songHTML}
      </div>
  </body>
  </html>`;
}

function getNonce() {
  let text = "";
  const possible =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (let i = 0; i < 32; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}
