# ChordMark for VSCode

ChordMark is a human-readable format for chords, lyrics and rhythm. It renders for excellent printing, too!

## Features

Preview your ChordMark files as you work. With your focus in an active text editor, press CTRL-SHIFT-`P` and select "ChordMark: HTML Preview" to open a preview to the side.

![Screenshot](<20231106 Screenshot.png>)

> This extension uses the same official libraries as Chord Chart Studio so you can be confident in the results.

## Extension Settings

This extension contributes no settings.

## Known Issues

Only one theme (`print`) is currently supported. Contact the developer to request more.

## Learn ChordMark

Credit to [Christophe Noël](https://github.com/no-chris/) for inventing the ChordMark format and open-sourcing the libraries.
* [Introduction to ChordMark](https://chordmark.netlify.app/)
* [ChordMark CSS themes](https://github.com/no-chris/chord-mark/tree/master/packages/chord-mark-themes)
* [Chord Chart Studio (online editor)](https:/chord-chart-studio.netlify.app/)

## Development

To run this extension for local development, 
1. `npm i` install dependencies from NPM
1. `npm hack` copy the chord-mark parser/renderer into the webview client (due to strict content security policies)
1. `npm scss` compile chord-mark-themes from SCSS to CSS
1. Press `F5` to start debugging the extension in VSCode.

<br/>

**Enjoy!**
